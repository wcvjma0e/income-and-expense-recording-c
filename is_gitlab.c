// Income & Expense Tracking Program
// By: jk105
#include <stdio.h>
#include <string.h>
#include <stdlib.h> // or free()
#include <ctype.h> // isdigit()
#include <errno.h> // perror()

char *month;
size_t month_size;
char *day;
size_t day_size;
char *iore;
size_t iore_size;
char incm_expns[8];
char *item;
size_t item_size;
char *category;
size_t category_size;
int within_rng;
/* Change with expense list */
int exp_len = 3;
/* List your expenses here */
char *catlist_exp[3] = {"food", "rent", "other"};
/* Change with income list */
int inc_len = 2;
/* List your income here */
char *catlist_inc[2] = {"salary", "other"};
char *cat;
char *amount;
size_t amount_size;
char isnumber;
FILE *spending;

int main(void) {
	/* Functions													*/
	/* ssize_t getline(char **lineptr, size_t *n, FILE *stream);	*/

	/* Month */
	month = NULL;
	month_size = 0;
	do {
		printf("Month:\n> ");
		if (getline(&month, &month_size, stdin) == -1) {
			/* Handle error with perror */
			perror("Getline() Error");
		}
		
		/* Trim newline character from getline() */
		month[strlen(month)-1] = '\0';

		/* Enforce month option to be within range */
		within_rng = atoi(month) >= 1 && atoi(month) <= 12;
		
	/* Repeat command when empty input or not within range */
	} while (strlen(month) == 0 || !within_rng);
	
	/* Day */
	day = NULL;
	day_size = 0;
	do {
		printf("Day:\n> ");
		if (getline(&day, &day_size, stdin) == -1) {
			/* Handle error with perror */
			perror("Getline() Error");
		}

		/* Trim newline character from getline() */
		day[strlen(day)-1] = '\0';
		
		/* Enforce day option to be within range */
		if (atoi(month) == 2 || atoi(month) == 4 || atoi(month) == 6 || atoi(month) == 9 || atoi(month) == 11) {	
			within_rng = atoi(day) >= 1 && atoi(day) <=30;
		} else {
			within_rng = atoi(day) >= 1 && atoi(day) <=31;
		}

	/* Repeat command when empty input or not within range */
	} while (strlen(day) == 0 || !within_rng);

	/* Income or Expense */
	iore = NULL;
	iore_size = 0;
	do {
		printf("Income(i) or Expense(e):\n> ");
		if(getline(&iore, &iore_size, stdin) == -1) {
			/* Handle error with perror */
			perror("Getline() Error");

		}

		/* Trim newline character from getline() */
		iore[strlen(iore)-1] = '\0';

	/* Repeat command when empty input or when input is not 'i' and 'e' */
	} while (strlen(iore) == 0 || (strcmp(iore, "i") != 0 && strcmp(iore, "e") != 0));

	/* Item */
	item = NULL;
	item_size = 0;
	do {
		printf("Item:\n> ");
		if(getline(&item, &item_size, stdin) == -1) {
			/* Handle error with perror */
			perror("Getline() Error");
		}

		/* Trim newline character from getline() */
		item[strlen(item)-1] = '\0';

		/* Repeat command when empty input */
	} while (strlen(item) == 0);

	/* Category */
	category = NULL;
	category_size = 0;

	do {
		if(strcmp(iore, "e") == 0) {

			printf("Category:\n");
			for(int i=0; i<exp_len; i++) {
				printf("(%d) %s\n", i, catlist_exp[i]);
			}
			printf("> ");
		} else if(strcmp(iore, "i") == 0) {
			printf("Category:\n");
			for(int i=0; i<inc_len; i++) {
				printf("(%d) %s\n", i, catlist_inc[i]);
			}
			printf("> ");
		}
		if(getline(&category, &category_size, stdin) == -1) {
			/* Handle error with perror */
			perror("Getline() Error");
		}
		/* Trim newline character from getline() */
		category[strlen(category)-1] = '\0';
		
		/* If category is not empty */
		if(strlen(category) != 0) {
			/* Enforce category option to be within range */
			if(strcmp(iore, "e") == 0) {
				within_rng = atoi(category) >= 0 && atoi(category) <= exp_len-1;
			} else if (strcmp(iore, "i") == 0) {
				within_rng = atoi(category) >= 0 && atoi(category) <= inc_len-1;
			}

			/* Enforce category option to be a number */
			isnumber = 1;
			for (int i=0; i<strlen(category); i++) {
				if(!isdigit(category[i])) {
					isnumber = 0;
				}
			}
			if (isnumber == 0) {
				printf("Enter a number\n");
			}
		}

	} while (strlen(category) == 0 || !within_rng || !isnumber);

	/* Amount */
	amount = NULL;
	amount_size = 0;
	do {
		printf("Amount:\n> ");
		if(getline(&amount, &amount_size, stdin) == -1) {
			/* Handle error with perror */
			perror("Getline() Error");
		}

		/* Trim newline character from getline() */
		amount[strlen(amount)-1] = '\0';

		/* If amount is not empty */
		if(strlen(amount) != 0) {
			/* Enforce category option to be a number */
			isnumber = 1;
			for (int i=0; i<strlen(amount); i++) {
				if(!isdigit(amount[i])) {
					/* Allow dot for decimal values */
					if((char) amount[i] != '.') {
						isnumber = 0;
					}
				}
			}
			if (isnumber == 0) {
				printf("Enter a number\n");
			}
		}
	} while (strlen(amount) == 0 || !isnumber);

	/* Text processing */
	if(strcmp(iore, "i") == 0) {
		strcpy(incm_expns, "income");
		cat = catlist_inc[atoi(category)];
	} else if (strcmp(iore, "e") == 0) {
		strcpy(incm_expns, "expense"); 
		cat = catlist_exp[atoi(category)];
	}

	/* Save results to file - spending.txt */
	spending = fopen("spending.txt", "a");

	if (strcmp(iore, "i") == 0) {
		printf("Saved: %s/%s, %s, %s, %s, %s\n", month, day, incm_expns, item, cat, amount);
		fprintf(spending, "%s/%s,%s,%s,%s,%s\n", month, day, incm_expns, item, cat, amount);
	} else if (strcmp(iore, "e") == 0) {
		printf("Saved: %s/%s, %s, %s, %s, -%s\n", month, day, incm_expns, item, cat, amount);
		fprintf(spending, "%s/%s,%s,%s,%s,-%s\n", month, day, incm_expns, item, cat, amount);
	}

	/* Close file */
	fclose(spending);

	/* Free memory space consumed by buffer */
	free(month);
	free(day);
	free(iore);
	free(item);
	free(category);
	free(amount);

	return 0;
}
